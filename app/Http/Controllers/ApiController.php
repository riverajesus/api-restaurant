<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiModel;

class ApiController extends Controller
{
    public function index(){
        // $model = new ApiModel;
        // $model->name = 'the first one';
        // $model->address = 'unknown';
        // $model->save();

        $model = ApiModel::all();
        return response()->json($model);
    }
}
