<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ApiModel extends Model
{
    protected $collection = 'restaurants';

    protected $fillable = ['name','address'];

    public $timestamps = false;
}