## Plantilla de laravel 9 con ORM y conexion a mongodb (jensseger)

### Requsitos
- php 8.2
  descargar extension de php para mongo
  https://github.com/mongodb/mongo-php-driver/releases/

- contenedor de mongo
  docker pull mongo
  sudo docker run --name mongodb -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=user -e MONGO_INITDB_ROOT_PASSWORD=pass mongo

- copiar archivo env
  cp .env.example .env

- instalar dependencias
  composer install

- generar llave
  php artisan key:generate

- correr proyecto
  php artisan serve